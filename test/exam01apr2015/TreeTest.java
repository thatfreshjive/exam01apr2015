
package exam01apr2015;

import org.junit.Test;
import static org.junit.Assert.*;


public class TreeTest {
    
    public TreeTest() {
    } // TreeTest()

    @Test
    public void testAdd_Record() {
        System.out.println("add");
        Record record = new Record("word1", 1);
        ByWord byword = new ByWord();
        Tree instance = new Tree(byword);
        instance.add(record);
        assertEquals(instance.root.getPayload(), record);
    } // testAdd_Record()

    @Test
    public void testAdd_Record_Node() {
        System.out.println("add");
        Record record = null;
        Node<Record> node = null;
        Tree instance = null;
        instance.add(record, node);
    } // testAdd_Record_Node()
    
} // TreeTest
