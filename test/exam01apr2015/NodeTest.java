
package exam01apr2015;

import org.junit.Test;
import static org.junit.Assert.*;

public class NodeTest {
    
    public NodeTest() {
    } // NodeTest()
    
    @Test
    public void testGetPayload() {
        System.out.println("getPayload");
        Record record = new Record("word1", 1);
        Node instance = new Node(record);
        Object expResult = record;
        Object result = instance.getPayload();
        assertEquals(expResult, result);
    } // testGetPayload()


    @Test
    public void testGetLeftChild() {
        System.out.println("getLeftChild");
        Record record1 = new Record("word1", 1);
        Record record2 = new Record("word2", 1);
        Node root = new Node(record);
        Node child = new Node(record);
        root.setLeftChild(child);
        Node expResult = child;
        Node result = instance.getLeftChild();
        assertEquals(expResult, result);
    } // testGetLeftChild()

    @Test
    public void testSetLeftChild() {
        System.out.println("setLeftChild");
        Record record1 = new Record("word1", 1);
        Record record2 = new Record("word2", 1);
        Node root = new Node(record);
        Node child = new Node(record);
        root.setLeftChild(child);
        Node expResult = child;
        Node result = instance.getLeftChild();
        assertEquals(expResult, result);
    } // testSetLeftChild()

    @Test
    public void testGetRightChild() {
        System.out.println("getRightChild");
        Record record1 = new Record("word1", 1);
        Record record2 = new Record("word2", 1);
        Node root = new Node(record);
        Node child = new Node(record);
        root.setRightchild();
        Node expResult = child;
        Node result = instance.getRightChild();
        assertEquals(expResult, result);
    } // testGetRightChild()

    @Test
    public void testSetRightChild() {
        System.out.println("setRightChild");
        Record record1 = new Record("word1", 1);
        Record record2 = new Record("word2", 1);
        Node root = new Node(record);
        Node child = new Node(record);
        root.setRightchild();
        Node expResult = child;
        Node result = instance.getRightChild();
        assertEquals(expResult, result);
    } // testRightChild()
    
} // NodeTest
