package exam01apr2015;

import org.junit.Test;
import static org.junit.Assert.*;

public class RecordTest {
    
    public RecordTest() {
    } // RecordTest()
    
    @Test
    public void testGetWord() {
        System.out.println("getWord");
        Record instance = new Record("word1", 1);
        String expResult = "word1";
        String result = instance.getWord();
        assertEquals(expResult, result);
    } // testGetWord()

    @Test
    public void testGetLocation() {
        System.out.println("getLocation");
        Record instance = new Record("word1", 6);
        Integer expResult = 6;
        Integer result = instance.getLocation();
        assertEquals(expResult, result);
    } // testGetLocation()

    @Test
    public void testGetCount() {
        System.out.println("getCount");
        Record instance = new Record("word1", 1);
        Integer expResult = 0;
        Integer result = instance.getCount();
        assertEquals(expResult, result);
    } // testGetCount()

    @Test
    public void testIncrementCount() {
        System.out.println("incrementCount");
        Record instance = new Record("word1", 1);
        instance.incrementCount();
        assertEquals(1, instance.count);
    } // testIncrementCount()

    
} // RecordTest
