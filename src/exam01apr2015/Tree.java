package exam01apr2015;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * This class models a binary search tree.
 * 
 * <br>
 * <img src="../../../build/classes/resources/Tree.png" alt="UML class diagram">
 * <br>
 * 
 * @author Isaac Asimov
 * @version 1 April 2015
 */
public class Tree {

    private Node<Record> root;
    private final Comparator<Record> comparator;
    
    /**
     * Initializes and empty Tree
     * @param The comparator class to be used when sorting records
     */
    public Tree(Comparator<Record> comparator) {
        this.root = null;
        this.comparator = comparator;
    } // Tree()

    /**
     * If the tree is empty, creates a root node holding the
     * given record, else it passes it to the secondary add method
     * @param record The record object to be inserted into a node
     */
    public void add(Record record) {
        if (this.root == null) {
            Node<Record> node = new Node(record);
            this.root = node;
        } // if
        else {
            this.add(record, this.root);
        } // else 
    } // add( Record )

    /**
     * Adds a record object to a node in an already existing tree of nodes.
     * this method attempts to resolve the correct location of the node
     * using the comparator specified upon initialization of the tree
     * @param record The record to be inserted into a node
     * @param node The root node for the particular 
     *         recursive traversial of the tree
     */
    public void add(Record record, Node<Record> node) {
        Record recordAtNode = node.getPayload();
        int code = this.comparator.compare(record, recordAtNode);
        if (code == 0) {
            recordAtNode.incrementCount();
        } // if
        else if (code < 0) {
            Node<Record> leftChild = node.getLeftChild();
            if (leftChild == null) {
                Node<Record> newNode = new Node(record);
                node.setLeftChild(newNode);
            } // if
            else {
                this.add(record, leftChild);
            } // else
        } // else if
        else if (code > 0) {
            Node<Record> rightChild = node.getRightChild();
            if (rightChild == null) {
                Node<Record> newNode = new Node(record);
                node.setRightChild(newNode);
            } // if
            else {
                this.add(record, rightChild);
            } // else
        } // else if
        else {
            // No other possibilities!
            // code must be equal to zero, negative, or positive
        } // else
    } // add( Record, Node<Record> )

    /**
     * Initial call to print the payload of the records in each node
     */
    public void print() {
        this.print( this.root );
    } // print()
    
    /**
     * Traverses the tree and recursively prints the payload of each
     * node contained within it
     * @param node to be printed in the current recursive iteration
     */
    public void print( Node<Record> node ) {
        if( node != null ) {
            this.print( node.getLeftChild());
            System.out.println( node.getPayload());
            this.print( node.getRightChild());
        } // if
    } // print( Node<Record> )
    
    /**
     * Creates an Array list of the tree's nodes
     * @return A list object populated with the contents of the tree
     */
    public List<Record> toList() {
        List<Record> list = new ArrayList<>();
        this.toList( this.root, list );
        return list;
    } // toList()
    
    /**
     * Recursively traverses the binary tree and populates the specified 
     * @param node
     * @param list 
     */
    public void toList( Node<Record> node, List<Record> list ) {
        if( node != null ) {
            this.toList( node.getLeftChild(), list );
            list.add( node.getPayload() );
            this.toList( node.getRightChild(), list);
        } // if
    } // toList( Node<Record> )
} // Tree<ElementType>
