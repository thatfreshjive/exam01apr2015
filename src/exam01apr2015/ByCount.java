package exam01apr2015;

import java.util.Comparator;


/**
 * A class for comparing the count value of two different records.
 * Implemented by the binary Tree class for comparing different nodes 
 */
public class ByCount implements Comparator<Record> {

    @Override
    public int compare(Record a, Record b) {
        int code = a.getCount().compareTo(b.getCount());
        if( code == 0 ) {
            return a.getWord().compareTo(b.getWord());
        } // if
        else {
            return code;
        } // else
    } // compare( Record, Record )
    
} // ByCount
