
package exam01apr2015;

import java.util.Comparator;

/**
 * A class for comparing the lexicographic values of the words contained in two records
 * Implemented by the binary Tree class for comparing different nodes 
 */
public class ByWord implements Comparator<Record> {

    /**
     * Constructs a comparator object that compares the lexicographic value
     * of the specified 
     * @param a The record object the is being compared
     * @param b The record object being compared to A
     * @return The lexicographic difference between the words in the records
     */
    @Override
    public int compare(Record a, Record b) {
        return a.getWord().compareTo(b.getWord());
    } // compare( Record, Record )
    
} // ByWord
