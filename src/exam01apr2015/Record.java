
package exam01apr2015;

/**
 * Instances of this class identify a word, the location
 * of its first occurrence in a document, and the number of
 * times that the word occurs in the document.
 * 
 * <br>
 * <img src="../../../build/classes/resources/Record.png" alt="UML class diagram">
 * <br>
 * 
 * @author Isaac Asimov
 * @version 1 April 2015
 */
public class Record {
    private final String word;
    private final Integer location;
    private Integer count;
    
    /**
     * Constructor for a new record object
     * @param word The word string this record is being created for
     * @param location The location in the text this word was first seen
     */
    public Record( String word, Integer location ) {
        this.word = word;
        this.location = location;
        this.count = 1;
    } // Record( String, Integer, Integer )

       /**
        * Getter for the word value of this record
        * @return the word string for this record
        */
    public String getWord() {
        return this.word;
    } // getWord()

    /**
     * Getter for the location value of this record
     * @return an integer representing the first occurance of this word 
     */
    public Integer getLocation() {
        return this.location;
    } // getLocation()

    /**
     * The number of times this word has been seen in the text
     * @return an integer representing the count of this word
     */
    public Integer getCount() {
        return this.count;
    } // getCount()

    /**
     * Increments this record's count value by one
     */
    public void incrementCount() {
        this.count++;
    } // incrementCount()
    
    /**
     * Returns a formatted string representation of all the values
     * contained in this particular record object
     * @return a formatted string with the records field values
     */
    @Override
    public String toString() {
        String result = String.format( "%20s (%5d) (%4d)", this.word,
                this.location, this.count);
        return result;
    } // toString()
} // Record
