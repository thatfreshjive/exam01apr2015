package exam01apr2015;

import java.util.Comparator;


public class ByLocation implements Comparator<Record> {

    @Override
    public int compare(Record a, Record b) {
        int code = a.getLocation().compareTo(b.getLocation());
        if( code == 0 ) {
            return a.getWord().compareTo(b.getWord());
        } // if
        else {
            return code;
        } // else
    } // compare( Record, Record )
    
} // ByLocation
