package exam01apr2015;

import com.opencsv.CSVWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
*
* @see <a href=http://michael.dipperstein.com/huffman/>Huffman Code Discussion and Implementation</a>
*/

public class Exam01apr2015 {

        private static final int MAX_WORDS = 10000;
        private static final int OUT_LENGTH = 10;
        private static final Logger logger
                = Logger.getLogger(Exam01apr2015.class.getName());

        public static void main(String[] args) throws IOException {
                String fileName = "siddhartha.txt";
                File file = new File(fileName);
                try {
                        Scanner scanner = new Scanner(file);

            // Skip over the identifying information at the
                        // front of the document. 
                        // Begin counting words only after the title
                        // of the book.
                        boolean foundStart = false;
                        while (scanner.hasNextLine() && !foundStart) {
                                String s = scanner.nextLine();
                                if (s.trim().equals("SIDDHARTHA")) {
                                        foundStart = true;
                                } // if
                        } // while

                        ByWord byWord = new ByWord();
                        Tree tree = new Tree(byWord);

            // Words contain only German letters.
                        // This regular expression means "any character that is not
                        // a German letter."
                        String regularExpression = "[^a-zA-ZäÄöÖüÜß]";
                        int location = 0;
                        while (scanner.hasNextLine() && location < MAX_WORDS) {
                                String s = scanner.nextLine();

                                // Words are separated by white space: "\\s"
                                String[] words = s.split("\\s");
                                for (String w : words) {
                                        String nakedWord
                                                = w.replaceAll(regularExpression, "").toLowerCase().trim();
                                        if (nakedWord.length() > 0) {
                                                Record record = new Record(nakedWord, location);
                                                tree.add(record);
                                                location++;
                                        } // if
                                } // for
                        } // while

                        // Print the records in alphabetical order.
                        System.out.println();
                        tree.print();
                        List<Record> list = tree.toList();

                        writeCSVOccurances(list, "occurances.csv");
            // Print the words in the order of the location
                        // of their first occurrence in the document.
                        //printListFirstOccurance(list, OUT_LENGTH);

            // Print the words in the order of the number of
                        // occurrences in the document.
                        //printListOccurances(list, OUT_LENGTH);
                } // try 
                catch (FileNotFoundException e) {
                        logger.log(Level.SEVERE, null, e);
                } // catch( FileNotFoundException )
        } // main( String [] )

        public static void writeCSVOccurances(List<Record> list, String filename) throws IOException {
                CSVWriter csv = new CSVWriter(new FileWriter(filename));
                ByCount bycount = new ByCount();
                Collections.sort(list, bycount);
                String[] header = "Word#Count#Location".split("#");
                csv.writeNext(header);
                for (Record r : list) {
                        String[] entry = (r.getWord() + "#" + r.getCount() + "#" + r.getLocation()).split("#");
                        csv.writeNext(entry);
                }
        }

        public static void printListOccurances(List<Record> list, int outLength) {
                System.out.println("Storted by descending frequency:");
                ByCount bycount = new ByCount();
                Collections.sort(list, bycount);
                if (outLength == 0) {
                        for (Record r : list) {
                                System.out.println(r);
                        }
                } else {
                        for (int i = 0; i < outLength; i++) {
                                System.out.println(list.get(i));
                        }
                }
        }

        public static void printListAlphabetical(List<Record> list, int outLength) {
                System.out.println("Sorted alphabetically:");
        }

        public static void printListFirstOccurance(List<Record> list, int outLength) {
                System.out.println("Sorted by first occurance in document:");
                ByLocation byLocation = new ByLocation();
                Collections.sort(list, byLocation);
                if (outLength == 0) {
                        for (Record r : list) {
                                System.out.println(r);
                        }
                } else {
                        for (int i = 0; i < outLength; i++) {
                                System.out.println(list.get(i));
                        }
                }
        }

} // Exam01apr2015
