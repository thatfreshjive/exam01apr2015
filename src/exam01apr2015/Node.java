
package exam01apr2015;

/**
 * This class models a node in a binary search tree.
 * 
 * <br>
 * <img src="../../../build/classes/resources/Node.png" alt="UML class diagram">
 * <br>
 * 
 * @author Isaac Asimov
 * @version 1 April 2015
 * @param <ElementType> is the kind of data that the tree contains.
 */
public class Node<ElementType> {
    private final ElementType payload;
    private Node leftChild;
    private Node rightChild;
    
    public Node( ElementType payload ) {
        this.payload = payload;
        this.leftChild = null;
        this.rightChild = null;
    } // Node( ElementType )

    public ElementType getPayload() {
        return this.payload;
    } // getPayload()

    public Node getLeftChild() {
        return this.leftChild;
    } // getLeftChild()

    public void setLeftChild(Node leftChild) {
        this.leftChild = leftChild;
    } // setLeftChild( Node )

    public Node getRightChild() {
        return this.rightChild;
    } // getRightChild()

    public void setRightChild(Node rightChild) {
        this.rightChild = rightChild;
    } // setRightChild( Node )
} // Node<ElementType>
